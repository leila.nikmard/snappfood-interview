import React from "react";
import VendorsList from "../../components/Vendor/VendorsList";

import "./style.scss"

const Restaurant = () => {
    return (
        <div className="container">
            <VendorsList />
        </div>
    )
}

export default Restaurant;
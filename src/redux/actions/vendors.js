import {ActionTypes} from "../actionTypes";

export const vendorsList = (data) => {
    return{
        type:ActionTypes.LOAD_ALL_VENDORS,
        payload: data
    }
}
import {combineReducers} from "redux";
import {vendorsReducer} from  "./vendors"

const rootReducer = combineReducers({
    vendors: vendorsReducer

});

export default rootReducer;
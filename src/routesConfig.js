import Restaurant from "./Pages/Restaurant";

const routesConfig = [
    {
        path:"/restaurant",
        component: Restaurant
    },
];

export default routesConfig;


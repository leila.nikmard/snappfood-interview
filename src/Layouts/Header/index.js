import Search from "../../components/Search/Search";
import React, { useEffect, useState } from "react";

import "./style.scss";
import Chip from "../../components/Chip";

const Header = () => {

    const [hideSearch , setHideSearch] = useState(false)
    const chips = [
        {name:"فیلترها"},
        {name: "دارای کوپن"},
        {name: "دارای تخفیف"}
    ];

    const handleScroll = (e) => {
        if(document.documentElement.scrollTop > 100){
            setHideSearch(true)
        }else{
            setHideSearch(false)
        }
    }
    
    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    },[]);
    return(
        <header>
            <div className={`search-box ${hideSearch ? "hideSearch" : ""}`}>
                <Search />
            </div>
            <div className="chips flex">
                {chips.map((it,index) => <div key={index}><Chip name={it.name}/></div>)}
            </div>
        </header>
    )
};

export default Header;
import React from "react";
import basketIcon from "../../img/basket.svg";
import backIcon from "../../img/back.svg";

import "./style.scss"

const Header = () => {
    return(
        <div className="navbar">
            <div className="back-navbar">
                <span>
                    <img src={backIcon}/>
                </span>
            </div>
            <div className="selected-position">
                <span>موقعیت انتخابی</span>
            </div>
            <div className="basket-navbar">
                <span className="back-icon">
                    <img src={basketIcon}/>
                </span>
            </div>
        </div>
    )
}

export default Header;
import React from "react";
import ordersIcon from "../../img/orders.svg";
import accountIcon from "../../img/account.svg";
import homeIcon from "../../img/orders.svg";

import "./style.scss"

const Navigation = () => {
    return(
        <div className="bottom-nav flex justify-content-start items-center">
                <div className="nav-link">
                    <div className="flex items-center justify-content-center flex-direction home">
                        <span>
                            <img src={homeIcon}/>
                        </span>
                        <span>خانه</span>
                    </div>
                </div>
                <div className="nav-link">
                    <div className="flex items-center justify-content-center flex-direction orders">
                        <span>
                            <img src={ordersIcon}/>
                        </span>
                        <span>سفارش ها</span>
                    </div>
                </div>
                <div className="nav-link">
                    <div className="flex items-center justify-content-center flex-direction account">
                        <span>
                            <img src={accountIcon} />
                        </span>
                        <span>حساب من</span>
                    </div>
                </div>
        </div>
    )

}

export default Navigation;
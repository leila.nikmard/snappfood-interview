import React from "react";

import "./style.scss";

const Search = () => {
  return (
    <input
      className="input-search"
      placeholder="جست و جو در اسنپ فود"
      type="text"
    />
  );
};

export default Search;

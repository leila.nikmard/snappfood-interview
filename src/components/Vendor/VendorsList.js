import React, {useEffect, useState,useCallback} from "react";
import VendorCard from "./VendorCard";
import {useSelector , useDispatch} from "react-redux";
import {vendorsList} from "../../redux/actions/vendors"
import {loadAllVendors} from "../../utils/services";
import VendorListHeader from "./VendorListHeader";
import { Virtuoso } from 'react-virtuoso';
import Loading from "../Loading";

import "./style.scss"



const VendorsList = () => {
    const dispatch = useDispatch();
    const [params , setParams] = useState({"page" : 1,
    "page-size" : 10,
    "lat": 35.754,
    "long": 51.328});
  const [loading , setLoading] = useState(false);
    const vendorsListData = useSelector(state => state?.vendors?.vendorsList);

    const loadVendors = async () => {
      setLoading(true);
        setParams({...params , "page":params["page"]+1})
        const response = await loadAllVendors(params);
        if(response.status){
          setLoading(false)
        }
        const data = response.data.data
        dispatch(vendorsList(data))
    }

    const loadMore = useCallback(() => {
        return loadVendors()
      }, [vendorsListData])
    
      useEffect(() => {
        loadMore()
      }, []);
      const vendorsOpenCount = vendorsListData[0];
      const VendorsData = vendorsListData.filter(it => it.type === "VENDOR")
    
    return (
      <>
        {loading ? <Loading/> : <>
          <VendorListHeader openCount={vendorsOpenCount}/>
          <Virtuoso
            style={{ height: "100vh" }}
            useWindowScroll
            data={VendorsData}
            endReached={loadMore}
            itemContent={(index, item) => (
              <VendorCard vendorData={item}/>
            )}
          />
        </>
        }
      </>
    )
}

export default VendorsList;
import * as config from "../config";
import axios from "axios"

export const loadAllVendors = (params) => axios.get(`${config.API_BASE_URL}/restaurant/vendors-list`,{
    params
});

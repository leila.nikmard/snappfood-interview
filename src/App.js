import React from "react";
import Header from "./Layouts/Header";
import NavBar from "./Layouts/NavBar";
import RouterComponent from "./routes";
import Navigation from "./Layouts/navigation";
import { StyleWrapper } from "./App.style";

function App() {
  return (
    <StyleWrapper>
      <NavBar/>
      <Header/>
      <RouterComponent />
      <Navigation />
    </StyleWrapper>
  );
}

export default App;
